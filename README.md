# IaC with Ansible project
- using local shell runner
- Ansible running on localhost
- Wiki example
- Webhook
- Snippet for ansible playbook change

Additional resources
- [Quick Webhook example with GitLab](https://dev.to/ciphercode/learn-how-to-use-webhooks-by-setting-up-a-gitlab-webhook-in-under-an-hour-50h2)
- [Managing your infrastructure with Ansible and GitLab CI/CD](https://medium.com/sopra-steria-norge/managing-your-infrastructure-with-ansible-and-gitlab-ci-cd-c820188270d6)
- [How to use GitLab and Ansible to create infrastructure as code](https://about.gitlab.com/blog/2019/07/01/using-ansible-and-gitlab-as-infrastructure-for-code/)
